Template.accountsList.helpers({
	accounts: function() {
		return Projects.find({members: Meteor.userId(), isAccount: true}, {sort: Session.get("sort_by")});
	}
});