Template.login.events({
	'submit .login-form': function(e) {
		e.preventDefault();
		
		var username = $('[name=username]').val();
        var password = $('[name=password]').val();
        Meteor.loginWithPassword(username, password, function(error){
    		console.log(error);
		});

		Router.go('dashBoard');
	}
});