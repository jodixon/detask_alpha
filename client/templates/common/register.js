Template.register.events({
	'submit .form-create-account': function(e) {
		e.preventDefault();

		var theUser = e.target.username.value;
		var thePassword = e.target.password.value;

		var newWorkGroup = {
			name: e.target.workGroupName.value,
			createdAt: new Date(),
			owner: null,
		};

		// temporary fix - research this!
		var theWorkGroup = Workgroups.insert(newWorkGroup);

		Accounts.createUser({
			username: theUser,
			password: thePassword,
			isAdmin: true,
			workGroupId: theWorkGroup,
		});

		Router.go('dashBoard');
	},
});