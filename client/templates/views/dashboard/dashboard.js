function rangeWeek (dateStr) {
   if (!dateStr) dateStr = new Date().getTime();
   var dt = new Date(dateStr);
   dt = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());
   dt = new Date(dt.getTime() - (dt.getDay() > 0 ? (dt.getDay() - 1) * 1000 * 60 * 60 * 24 : 6 * 1000 * 60 * 60 * 24));
   return { start: dt, end: new Date(dt.getTime() + 1000 * 60 * 60 * 24 * 7 - 1) };
}

var currentDate = new Date();
var todayString = currentDate.getDate().toString() + currentDate.getMonth().toString() + currentDate.getFullYear().toString();
currentDate.setDate(currentDate.getDate() + 1);
var tomorrowString = currentDate.getDate().toString() + currentDate.getMonth().toString() + currentDate.getFullYear().toString();

// var weekRange = rangeWeek();
// var startDate = weekRange.start;
// var endDate = weekRange.end;

// var startWeek = moment(startDate);
// var endWeek = moment(endDate);

// var startWeekString = startWeek.format("MMMM M");
// var endWeekString = endWeek.format("MMMM M");

Session.set("dashboard_tasks_due_by_filter", {
	dueByDateString: todayString,
	completed: false
});

Session.set("active_tasks_filter", "Due Today");


Template.dashBoard.onCreated(function() {
	var self = this;
	self.autorun(function() {
		self.subscribe("projects");
		self.subscribe("stories");
	});
});

Template.dashBoard.helpers({
	dashBoardTasks: function() {
		return Tasks.find(Session.get("dashboard_tasks_due_by_filter"));
	},

	activeTasksFilter: function() {
		return Session.get("active_tasks_filter");
	},

	emails : function() {
		var weekRange = rangeWeek();
		var startDate = weekRange.start;
		var endDate = weekRange.end;

		return Tasks.find({completed: true, taskType: "email", dateFinished: {$gte: startDate, $lte: endDate}});

	},

	emailCount: function() {
		var weekRange = rangeWeek();
		var startDate = weekRange.start;
		var endDate = weekRange.end;

		return Tasks.find({completed: true, taskType: "email", dateFinished: {$gte: startDate, $lte: endDate}}).count();
	},

	calls : function() {
		var weekRange = rangeWeek();
		var startDate = weekRange.start;
		var endDate = weekRange.end;

		return Tasks.find({completed: true, taskType: "call", dateFinished: {$gte: startDate, $lte: endDate}});

	},

	callCount: function() {
		var weekRange = rangeWeek();
		var startDate = weekRange.start;
		var endDate = weekRange.end;

		return Tasks.find({completed: true, taskType: "call", dateFinished: {$gte: startDate, $lte: endDate}}).count();
	},

	feed: function() {

		var theFeed = [];
		var userProjects = Projects.find({members: Meteor.userId()});
		console.log(userProjects.count());
		userProjects.forEach(function(project) {
			var stories = Stories.find({projectId: project._id});
			stories.forEach(function(story) {
				theFeed.push(story);
			});
		});
		return _.sortBy(theFeed, 'lastUpdated').reverse();

	},

	startWeekString: function() {

		var weekRange = rangeWeek();
		var startDate = weekRange.start;
		var startWeek = moment(startDate).format("MMMM Do");

		return startWeek;

	},

	endWeekString: function() {

		var weekRange = rangeWeek();
		var endDate = weekRange.end;
		var endWeek = moment(endDate).format("MMMM Do");
		
		return endWeek;

	},

	visits : function() {
		var weekRange = rangeWeek();
		var startDate = weekRange.start;
		var endDate = weekRange.end;

		return Tasks.find({completed: true, taskType: "visit", dateFinished: {$gte: startDate, $lte: endDate}});

	},

	visitCount: function() {
		var weekRange = rangeWeek();
		var startDate = weekRange.start;
		var endDate = weekRange.end;

		return Tasks.find({completed: true, taskType: "visit", dateFinished: {$gte: startDate, $lte: endDate}}).count();
	}

});

Template.dashBoard.events({
	'click .today': function() {
		Session.set("dashboard_tasks_due_by_filter", {
			dueByDateString: todayString,
			completed: false
		});
		Session.set("active_tasks_filter", "Due Today");
	},

	'click .tomorrow': function() {
		Session.set("dashboard_tasks_due_by_filter", {
			dueByDateString: tomorrowString,
			completed: false
		});
		Session.set("active_tasks_filter", "Due Tomorrow");
	},

	'click .overdue': function() {
		Session.set("dashboard_tasks_due_by_filter", {
			dueByDate: {$lt: currentDate},
			completed: false
		});
		Session.set("active_tasks_filter", "Overdue");
	}
});