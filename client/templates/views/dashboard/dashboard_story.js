Template.dashboardStoryItem.helpers({

	projectLink: function() {
		return Projects.findOne({_id: this.projectId}).title;
	},

	body: function() {
		return this.body;
	},

	checked: function() {
		if (this.completed) {
			return "checked";
		}
	},

	dateCreated: function() {
		var timeStamp = moment(this.createdAt).fromNow();
		return timeStamp;
	},

	dateString: function() {
		// var timeStamp = moment(this.lastUpdated).format("MMM Mo");
		var timeStamp = moment(this.lastUpdated).fromNow();
		return timeStamp;
	},

	fileName: function() {
		return this.fileReference.name();
	},

	hasReplyStatus: function() {
		return this.hasReplyStatus;
	},

	isFile: function() {
		return this.documentType === "file";
	},

	isMessage: function() {
		return this.documentType === "message";
	},

	isTask: function() {
		return this.documentType === "task";
	},

	notes: function() {
		return Notes.find({storyId: this._id});
	},

	ownStory: function() {
		return this.userId === Meteor.userId();
	},

	replyStatusAction: function() {
		return this.replyStatus.actionString;
	},

	replyCount: function() {
		return Notes.find({storyId: this._id}).count();
	},

	replyStatusAuthor: function() {
		return this.replyStatus.author;
	},

	replyStatusDate: function() {
		var replyDate =  moment(this.replyStatus.createdAt).fromNow();
		return replyDate;
	},

});