Template.fileStub.helpers({
	'fileSize': function() {
		var totalBytes = this.size();
		return totalBytes / 1000;
	}

	// 'username': function() {
	// 	return Meteor.users.find({_id: this.owner}).username;
	// }
});