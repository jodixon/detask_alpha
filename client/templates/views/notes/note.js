Template.note.helpers({
	createdAt: function() {
		return moment(this.created).fromNow();
	}
});
