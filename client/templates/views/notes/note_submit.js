Template.noteSubmit.events({
	'submit form': function(e, template) {
		e.preventDefault();

		// create the subtask object
		var note = {
			body: $(e.target).find('[name=body]').val(),
			taskId: template.data._id
		};

		Meteor.call('noteInsert', note, function(error, result) {
			// display the error to the user and abort
			if (error)
				return alert(error.reason);

			Modal.hide();
		});
	}
});