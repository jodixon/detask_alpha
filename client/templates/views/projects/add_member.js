Template.addMember.events({
	'submit form': function(e, template) {
		e.preventDefault();

		var newMember = {
			username: $(e.target).find('[name=username]').val(),
			projectId: template.data._id
		};

		Meteor.call('memberInsert', newMember, function(error, result) {
			// display the error to the user and abort
			if (error)
				return alert(error.reason);
			Modal.hide();
		});
	}
});