Template.leadImport.events({
	'change .lead-import': function(evt, tmpl) {
		FS.Utility.eachFile(event, function(file) {
         // parse CSV file
         Papa.parse(file, {
         	complete: function(results) {
         		// for each element in results, create a new lead
               $.each(results.data, function(index, value){

                  var lead = {
                     companyName: value[0],
                     contactFirstName: value[1],
                     contactLastName: value[2],
                     email: value[3],
                     phone: value[4],
                     address: value[5],
                     city: value[6],
                     state: value[7],
                     zipCode: value[8]
                  };

                  Meteor.call('projectInsert', lead, function(error, result) {
                     if (error)
                        return alert(error.reason);
                  });

               });

         	}	
         });
		});
      Modal.hide();
	}
});