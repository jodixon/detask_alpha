Template.projectItem.helpers({
	ownProject: function() {
		return this.userId === Meteor.userId();
	},

	activeLeadTasks: function() {
		return Tasks.find({projectId: this._id, completed: false})
	}
});

Template.projectItem.events({

	'click .project-edit': function() {
		Modal.show('taskEdit', Tasks.findOne({_id: this._id}));
	},

	'click .project-delete': function(e) {
		e.preventDefault();

		if (confirm('Delete this task?')) {
			var currentTaskId = this._id;
			Tasks.remove(currentTaskId);
		}
	},

	'click .convert-account': function() {
		Meteor.call('convertToAccount', this._id, function(error, result) {
			// display the error to the user and abort
				if (error)
					return alert(error.reason);
		});
	}

});