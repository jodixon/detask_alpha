Template.projectPage.onCreated(function() {
	var self = this;
	self.autorun(function() {
		self.subscribe("projects");
		self.subscribe("stories");
	});
});

Template.projectPage.helpers({
	tasks: function() {
		return Tasks.find({projectId: this._id, completed: false}, {sort: {dueByDate: 1}});
	},

	completedTasks: function() {
		return Tasks.find({projectId: this._id, completed: true});
	},

	ownProject: function() {
		return this.userId === Meteor.userId();
	},

	// comments: function() {
	// 	return Comments.find({projectId: this._id}, {sort: {date: -1}});
	// },

	tasksCount: function() {
		return Tasks.find({projectId: this._id, completed: false}).count();
	},

	completedTasksCount: function() {
		return Tasks.find({projectId: this._id, completed: true}).count();
	},

	members: function() {
		var results = [];
		$.each(this.members, function(index, value) {
			results.push(Meteor.users.findOne(value).username);
		});
		return results;
	},

	stories: function() {
		return Stories.find({projectId: this._id}, {sort: {lastUpdated: -1}});
	}

	// files: function() {
	// 	return Resources.find({projectId: this._id});
	// }, 

	// filesCount: function() {
	// 	return Resources.find({projectId: this._id}).count();
	// }
});

Template.projectPage.events({
	'click .new-task': function() {
		Modal.show('taskSubmit', Projects.findOne({_id: this._id}));
	},

	'submit form.new-story': function(e, template) {
		e.preventDefault();

		var story = {
			// message specific attributes
			documentType: "message",
			body: $(e.target).find('[name=body]').val(),

			// story attributes
			projectId: template.data._id,
			details: "added a new",
			tags: [],
			hasReplyStatus: false,
			replyStatus: {}
		};

		Meteor.call('storyInsert', story, function(error, result) {
			if (error)
				return alert(error.reason);
		});
	},

	'click .new-file': function() {
		Modal.show('taskFileSubmit', Projects.findOne({_id: this._id}));
	},

	'click .add-member': function() {
		Modal.show('addMember', Projects.findOne({_id: this._id}));
	}

});