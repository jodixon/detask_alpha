Template.projectSubmit.events({
	'submit form': function(e) {
		e.preventDefault();

		var project = {
			title: $(e.target).find('[name=title]').val(),
			description: $(e.target).find('[name=description]').val()
		};

		Meteor.call('projectInsert', project, function(error, result) {
			// display the error to the user and abort
			if (error)
				return alert(error.reason);
			Modal.hide();
		});
	},

	'click .cancel': function() {
		Modal.hide();
	}
});