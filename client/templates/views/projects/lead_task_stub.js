Template.leadTaskStub.helpers({
	dueBy: function() {
		var d = Tasks.find({_id: this._id}).dueByDate;
		return moment(d).format('ddd D');
	}
});