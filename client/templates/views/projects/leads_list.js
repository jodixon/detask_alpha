Session.set("sort_by", {
	title: 1,
});

Template.projectsList.onCreated(function() {
	var self = this;
	self.autorun(function() {
		self.subscribe("projects");
	});
});

Template.projectsList.helpers({
	projects: function() {
		return Projects.find({members: Meteor.userId(), isAccount: false}, {sort: Session.get("sort_by")});
	},

	projectsIndex: function() {
		return ProjectsIndex;
	},

	inputAttributes: function () {
      return { 'class': 'easy-search-input form-control', 'placeholder': 'Search leads' };
    }
});

Template.projectsList.events({
	'click .new-project': function() {
		Modal.show('projectSubmit');
	},

	'click .import': function() {
		Modal.show('leadImport');
	},

	'click .sort-name': function() {
		Session.set("sort_by", {companyName: 1});
	},

	'click .import-csv': function() {
		Modal.show('leadImport');
	}
});