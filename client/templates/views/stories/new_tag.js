Template.tagSubmit.events({
	'submit form': function(e) {
		e.preventDefault();

		var tag = {
			tagContent: $(e.target).find('[name=body]').val(),
			storyId: this._id
		};

		Meteor.call("tagInsert", tag, function(error, result) {
			if (error)
				return alert(error.reason);
		});
	}
});