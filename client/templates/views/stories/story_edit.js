Template.storyEdit.events({

	'submit form': function(e) {
		e.preventDefault();

		var currentStory = Stories.findOne({_id: this._id});
		var currentStoryId = this._id;

		var storyProperties = {
			body: $(e.target).find('[name=body]').val(),
		}

		Stories.update(currentStoryId, {$set: storyProperties}, function(error) {
			if (error) {
				// display the error message
				alert(error.reason);
			} else {
				Modal.hide();
			}
		});
	},

});