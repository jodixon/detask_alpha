Template.storyItem.onCreated(function storyItemOnCreated() {
	Meteor.subscribe('notes');
});

	Template.storyItem.onCreated(function() {
		Meteor.subscribe('tags');
	});

Template.storyItem.helpers({
	body: function() {
		return this.body;
	},

	checked: function() {
		if (this.completed) {
			return "checked";
		}
	},

	dateCreated: function() {
		var timeStamp = moment(this.createdAt).fromNow();
		return timeStamp;
	},

	dateString: function() {
		// var timeStamp = moment(this.lastUpdated).format("MMM Mo");
		var timeStamp = moment(this.lastUpdated).fromNow();
		return timeStamp;
	},

	fileName: function() {
		return this.fileReference.name();
	},

	hasReplyStatus: function() {
		return this.hasReplyStatus;
	},

	isFile: function() {
		return this.documentType === "file";
	},

	isMessage: function() {
		return this.documentType === "message";
	},

	isTask: function() {
		return this.documentType === "task";
	},

	notes: function() {
		return Notes.find({storyId: this._id});
	},

	ownStory: function() {
		return this.userId === Meteor.userId();
	},

	replyStatusAction: function() {
		return this.replyStatus.actionString;
	},

	replyCount: function() {
		return Notes.find({storyId: this._id}).count();
	},

	replyStatusAuthor: function() {
		return this.replyStatus.author;
	},

	replyStatusDate: function() {
		var replyDate =  moment(this.replyStatus.createdAt).fromNow();
		return replyDate;
	},

	tags: function() {
		return Tags.find({storyId: this._id});
	}
});


Template.storyItem.events({
	'submit form.new-reply': function(e, template) {
		e.preventDefault();



		// create the subtask object
		var note = {
			body: $(e.target).find('[name=body]').val(),
			storyId: template.data._id
		};

		Meteor.call('noteInsert', note, function(error, result) {
			// display the error to the user and abort
			if (error)
				return alert(error.reason);
			
		});

		var newReply = {
			storyId: template.data._id
		};

		Meteor.call('replyToStory', newReply, function(error, result) {
			if (error)
				return alert(error.reason);
		});

		// clear input
		$('form.new-reply').reset();
	},

	'change input.complete-task': function(e, template) {

		var thisStory = Stories.findOne({_id: template.data._id});

		Meteor.call("taskComplete", thisStory, function(error, result) {
			if (error)
				return alert(error.reason);
		});

		console.log(thisStory.completed);

		var newReply = {
			storyId: template.data._id
		};

		Meteor.call("completedTaskStoryUpdate", newReply, function(error, result) {
			if (error)
				return alert(error.reason);
		});
	},

	'click .story-edit': function() {
		Modal.show('storyEdit', Stories.findOne({_id: this._id}));
	},

	'click .story-delete': function(e) {
		e.preventDefault();

		if (confirm('Delete this story?')) {
			var currentStoryId = this._id;
			Stories.remove(currentStoryId);
		}
	},

	'click .add-tag': function() {
		Modal.show('tagSubmit', Stories.findOne({_id: this._id}));
	},

});