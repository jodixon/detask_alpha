Template.confirmComplete.events({
	'click .confirm': function() {
		Meteor.call('taskComplete', Tasks.findOne({_id: this._id}), function(error, result) {
			// display the error to the user and abort
				if (error)
					return alert(error.reason);
		});
	},

	'click .cancel': function() {
		Modal.hide();
	}
});