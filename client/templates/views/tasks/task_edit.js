Template.taskEdit.events({
	'submit form': function(e) {
		e.preventDefault();

		var currentTask = Tasks.findOne({_id: this._id});
		var currentTaskId = this._id;

		var taskProperties = {
			taskType: $(e.target).find('[name=taskType]').val(),
			title: $(e.target).find('[name=title]').val(),
			description: $(e.target).find('[name=description]').val()
		}

		Tasks.update(currentTaskId, {$set: taskProperties}, function(error) {
			if (error) {
				// display the error message
				alert(error.reason);
			} else {
				Modal.hide();
			}
		});
	},

	'click .delete': function(e) {
		e.preventDefault();

		if (confirm('Delete this task?')) {
			var currentTaskId = this._id;
			Tasks.remove(currentTaskId);
			Modal.hide();
		}
	}
});

Template.taskEdit.onRendered(function() {

	// Activate Selectize jQuery plugin
	$('#task-type-select').selectize({
		create: true,
		sortField: {field: 'text'}
	});

	$('#datepicker').datepicker();

});