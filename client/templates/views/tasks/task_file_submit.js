Template.taskFileSubmit.events({
	'change .filestyle': function(event, template) {
        FS.Utility.eachFile(event, function(file) {
            
            var yourFile = new FS.File(file);
            yourFile.owner = Meteor.user();
            yourFile.projectId = template.data._id;
            yourFile.date = new Date().toDateString();

            var fileObj = Resources.insert(yourFile);
            console.log(fileObj);

            var story = {
                // file specific attributes
                documentType: "file",
                fileReference: fileObj,

                // story attributes
                projectId: template.data._id,
                details: "added a new",
                hasReplyStatus: false,
                replyStatus: {}
            };

            Meteor.call('storyInsert', story, function(error, result) {
                if (error)
                    return alert(error.reason);
            });

        });
        
        Modal.hide();
    }
});