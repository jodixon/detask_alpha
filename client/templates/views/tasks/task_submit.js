Template.taskSubmit.events({
	'submit form': function(e, template) {
		e.preventDefault();

		// get due date (optional)
		var d = $(e.target).find('[name=datepicker]').val();
		var dueBy = new Date(d);

		var story = {

			//task specific attributes
			documentType: "task",
			dueByDate: dueBy,
			completed: false,
			dueByDateString: dueBy.getDate().toString() + dueBy.getMonth().toString() + dueBy.getFullYear().toString(),
			body: $(e.target).find('[name=subject]').val(),

			// story attributes
			projectId: template.data._id,
			details: "added a new",
			hasReplyStatus: false,
			replyStatus: {}
		};

		Meteor.call('storyInsert', story, function(error, result) {
			if (error)
				return alert(error.reason);
		});

	},

	'click .cancel': function() {
		Modal.hide();
	}
});


Template.taskSubmit.onRendered(function() {

	
	// Activate Datepicker jQuery plugin
	$('#datepicker').datepicker();

	// Activate Selectize jQuery plugin
	$('#task-type-select').selectize({
		create: true,
		sortField: {field: 'text'}
	});

});