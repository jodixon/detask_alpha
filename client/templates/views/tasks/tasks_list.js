Session.set("tasks_sort_by", {
	dueByDate: -1,
});

Session.set("tasks_filter", {
	userId: Meteor.userId(),
	completed: false
});

Template.tasksList.helpers({

	tasks: function() {
		return Tasks.find(Session.get("tasks_filter"), {sort: Session.get("tasks_sort_by")});
	}

});

Template.tasksList.events({
	
	'click .sort-name': function() {
		Session.set("tasks_sort_by", {title: 1});
	},

	'click .sort-date': function() {
		Session.set("tasks_sort_by", {dueByDate: 1});
	},

	'click .filter-active': function() {
		Session.set("tasks_filter", {
			userId: Meteor.userId(),
			completed: false
		});
		console.log(Session.get("tasks_filter"));
	},

	'click .filter-completed': function() {
		Session.set("tasks_filter", {
			userId: Meteor.userId(),
			completed: true
		});
		console.log(Session.get("tasks_filter"));
	}

});