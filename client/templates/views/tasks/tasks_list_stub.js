Template.tasksListStub.helpers({
	taskCompanyName: function() {
		return Projects.findOne({_id: this.projectId}).companyName;
	},

	ownTask: function() {
		return this.userId === Meteor.userId();
	},

	// subtasks: function() {
	// 	return Subtasks.find({taskId: this._id});
	// },

	// files: function() {
	// 	return Resources.find({taskId: this._id});
	// },

	isNotCompleted: function() {
		return this.completed === false;
	},

	date: function() {
		return moment(this.dueByDate).format('ddd MMM Do');
	},

	dateCompleted: function() {
		return moment(this.dateFinished).format('ddd MMM Do');
	},

	files: function() {
		return Resources.find({taskId: this._id});
	},

	notes: function() {
		return Notes.find({taskId: this._id});
	}
});

Template.tasksListStub.events({
	'click .task-edit': function() {
		Modal.show('taskEdit', Tasks.findOne({_id: this._id}));
	},

	'click .task-delete': function(e) {
		e.preventDefault();

		if (confirm('Delete this task?')) {
			var currentTaskId = this._id;
			Tasks.remove(currentTaskId);
		}
	},

	'click .task-complete': function() {

		if (this.completed === false) {
			Meteor.call('taskComplete', Tasks.findOne({_id: this._id}), function(error, result) {
			// display the error to the user and abort
				if (error)
					return alert(error.reason);
			});
		} else {
			Meteor.call('taskUnComplete', Tasks.findOne({_id: this._id}), function(error, result) {
			// display the error to the user and abort
				if (error)
					return alert(error.reason);
			});
		}

	},

	'click .new-file': function() {
		Modal.show('taskFileSubmit', Tasks.findOne({_id: this._id}));
	},

	'submit form': function(e, template) {
		e.preventDefault();

		// create the subtask object
		var note = {
			body: $(e.target).find('[name=body]').val(),
			taskId: template.data._id
		};

		Meteor.call('noteInsert', note, function(error, result) {
			// display the error to the user and abort
			if (error)
				return alert(error.reason);
			
		});
	}

});