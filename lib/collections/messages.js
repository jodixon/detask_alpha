Messages = new Mongo.Collection('messages');

Meteor.methods({
	messageInsert: function(messageAttributes) {
		var user = Meteor.user();

		// extend the task object
		var message = _.extend(messageAttributes, {
			created: new Date(),
			authorId: user._id,
			author: user.username,
		});

		var messageId = Messages.insert(message);

		return {
			_id: messageId
		};
	}
});