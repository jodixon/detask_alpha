Notes = new Mongo.Collection('notes');

Notes.allow({
	// need to set permissions
	update: function(userId, note) { return ownsDocument(userId, note); },
	remove: function(userId, note) { return ownsDocument(userId, note); } 
});

Meteor.methods({
	noteInsert: function(noteAttributes) {
	
		var user = Meteor.user();

		// extend the note object
		var note = _.extend(noteAttributes, {
			userId: user._id,
			userName: user.username,
			created: new Date()
		});

		var noteId = Notes.insert(note);

		return {
			_id: noteId
		};
	}
});