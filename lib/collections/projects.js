Projects = new Mongo.Collection('projects');

ProjectsIndex = new EasySearch.Index({
	engine: new EasySearch.Minimongo(),
	collection: Projects,
	fields: ['companyName']
});

Projects.allow({
	update: function(userId, project) { return ownsDocument(userId, project); },
	remove: function(userId, project) { return ownsDocument(userId, project); } 
});

Meteor.methods({
	projectInsert: function(projectAttributes) {

		var project = _.extend(projectAttributes, {
			created: new Date(),
			userId: Meteor.userId(),
			author: Meteor.user().username,
			isAccount: false,
			members: [Meteor.userId()]
		});

		if (!project)
			throw new Meteor.Error('invalid-project', 'You must enter a message');

		var projectId = Projects.insert(project);

		console.log(project.members);

		return {
			_id: projectId
		};
	},

	memberInsert: function(newMember) {

		if (!newMember)
			throw new Meteor.Error('invalid-member', 'Enter a username');

		Projects.update(newMember.projectId, {$push: {members: newMember.username}});

	},

	convertToAccount: function(projectId) {
		Projects.update({
			_id: projectId
		}, {
			$set: {isAccount: true}
		});
	}
});