var resourceStore = new FS.Store.GridFS("resources");

Resources = new FS.Collection("resources", {
	stores: [resourceStore]
});

Resources.allow({
	update: function() { 
		return true; 
	},

	insert: function() {
		return true;
	}
});