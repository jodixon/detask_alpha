Stories = new Mongo.Collection('stories');

Stories.allow({
	update: function(userId, story) { return ownsDocument(userId, story); },
	remove: function(userId, story) { return ownsDocument(userId, story); } 
});

Meteor.methods({
	
	storyInsert: function(storyAttributes) {
		
		var user = Meteor.user();

		var story = _.extend(storyAttributes, {
			createdAt: new Date(),
			userId: user._id,
			author: user.username,
			lastUpdated: new Date(),

		});

		var storyId = Stories.insert(story);

		return {
			_id: storyId
		};


	},

	replyToStory: function(replyAttributes) {
		var user = Meteor.user();

		var newReplyStatus = _.extend(replyAttributes, {
			userId: user._id,
			author: user.username,
			replyCreatedAt: new Date(),
			actionString: "added a reply",
		});

		Stories.update({
			_id: newReplyStatus.storyId,
		}, {
		  $set: {hasReplyStatus: true, replyStatus: newReplyStatus, lastUpdated: new Date()}
		});


	},

	taskComplete: function(story) {
		Stories.update({
			_id: story._id,
		}, {
			$set: {completed: true, dateFinished: new Date()}
		});
	},

	taskUnComplete: function(task) {
		Stories.update({
			_id: task._id,
		}, {
			$set: {completed: false, dateFinished: null}
		});
	},

	completedTaskStoryUpdate: function(replyAttributes) {
		var user = Meteor.user();

		var newReplyStatus = _.extend(replyAttributes, {
			userId: user._id,
			author: user.username,
			createdAt: new Date(),
			actionString: "completed a task",
		});

		Stories.update({
			_id: newReplyStatus.storyId,
		}, {
		  $set: {hasReplyStatus: true, replyStatus: newReplyStatus, lastUpdated: new Date()}
		});

	},

	addTag: function(tagAttributes) {
		var user = Meteor.users();

		var newTag = _.extend(tagAttributes,  {
			userId: user._id,
			author: user.username,
			createdAt: new Date(),
		});

		Stories.update({
			_id: tagAttributes.storyId,
		}, {

			$push: {tags: tagAttributes.tagLabel} 
		});

	},

});