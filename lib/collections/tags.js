Tags = new Mongo.Collection('tags');

Tags.allow({
	update: function() { return true; },
	remove: function() { return true; } 
});

Meteor.methods({
	tagInsert: function(tagAttributes) {

		var user = Meteor.user();

		// extend the task object
		var tag = _.extend(tagAttributes, {
			created: new Date(),
			userId: user._id,
			author: user.username,
			workGroupId: user.workGroupId
		});

		var tagId = Tags.insert(tag);

		return {
			_id: tagId
		};
	}
});