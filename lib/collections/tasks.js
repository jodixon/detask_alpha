Tasks = new Mongo.Collection('tasks');

Tasks.allow({
	update: function(userId, task) { return ownsDocument(userId, task); },
	remove: function(userId, task) { return ownsDocument(userId, task); } 
});

Meteor.methods({
	taskInsert: function(taskAttributes) {

		var user = Meteor.user();

		// extend the task object
		var task = _.extend(taskAttributes, {
			created: new Date(),
			userId: user._id,
			author: user.username,
			completed: false,
		});

		var taskId = Tasks.insert(task);

		return {
			_id: taskId
		};
	}
});