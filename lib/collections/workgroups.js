Workgroups = new Mongo.Collection('workgroups');

Workgroups.allow({

  insert: function() {
    return true;
  },
  
});

Meteor.methods({

	workGroupInsert: function(workGroupAttributes) {

		var workGroup = _.extend(workGroupAttributes, {
			createdAt: new Date(),
			ownerId: null,
		});

		var workGroupId = Workgroups.insert(workGroup);

		return {
			_id: workGroupId,
		};


	}

});