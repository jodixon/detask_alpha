Router.onBeforeAction(function () {

  if (!Meteor.userId()) {
    // if the user is not logged in, render the Login template
    this.render('login');
  } else {
    // otherwise don't hold up the rest of hooks or our route/action function
    // from running
    this.next();
  }
});

Router.configure({
  // the default layout
  layoutTemplate: 'layout'
});

Router.route('/', {
	// this.layout('layout');
	// this.render('dashBoard', {name: 'dashBoard'});
	// this.render('topnavbar', {to: 'navbar'});
	// this.render('sidebar', {to: 'sidenav'});

	layoutTemplate: 'layout',
	name: 'dashBoard',
	template: 'dashBoard',
	yieldRegions: {
		'topnavbar': {to: 'navbar'},
		'sidebar': {to: 'sidenav'},
		'contextMenu': {to: 'contextMenu'}
	}
});

Router.route('/workgroups/:id', function() {
	this.layout('layout');
	this.render('WorkGroupDashboard', {
    	data: function () {
    		var userWorkGroupId = Meteor.user().workGroupId;
      		return Workgroups.findOne({_id: userWorkGroupId});
    	}
  	});
	this.render('topnavbar', {to: 'navbar'});
	this.render('sidebar', {to: 'sidenav'});
});

Router.route('/register', function() {
	this.layout('PublicLayout');
	this.render('register');
});

Router.route('/login', function() {
	this.layout('PublicLayout');
	this.render('login');
});

Router.route('/projects', {
	name: 'projectsList',
	layoutTemplate: 'layout',
	template: 'projectsList',
	yieldRegions: {
		'topnavbar': {to: 'navbar'},
		'sidebar': {to: 'sidenav'}
	}
});

Router.route('/projects/:_id', {
	layoutTemplate: 'layout',
	name: 'projectPage',
	template: 'projectPage',
	data: function() { return Projects.findOne(this.params._id); },
	yieldRegions: {
		'topnavbar': {to: 'navbar'},
		'sidebar': {to: 'sidenav'},
		'contextMenu': {to: "contextMenu"}

	}
});

Router.route('/tasks', {name: 'tasksList'});



