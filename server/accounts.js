Accounts.onCreateUser(function(options, user) {

   // Use provided profile in options, or create an empty object
   user.isAdmin = options.isAdmin;
   console.log(user.isAdmin);

   user.workGroupId = options.workGroupId;
   console.log(user.workGroupId);

   // Returns the user object
   return user;
});