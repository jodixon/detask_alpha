Meteor.publish('stories', function storiesPublication() {
	return Stories.find({});
});

Meteor.publish('projects', function projectsPublication() {
	return Projects.find({});
});

Meteor.publish('notes', function notesPublication() {
	return Notes.find({});
});

Meteor.publish("userData", function () {
  if (this.userId) {
    return Meteor.users.find({_id: this.userId},
                             {fields: {'isAdmin': 1, 'workGroupId': 1}});
  } else {
    this.ready();
  }
});

Meteor.publish('workGroups', function workGroupsPublication() {
	return Workgroups.find({});
});

Meteor.publish('tags', function tagsPublication() {
  return Tags.find({});
});
